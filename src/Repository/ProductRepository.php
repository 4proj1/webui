<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
//use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    /**
     * ProductRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return Query
     */
    public function findEmptyQuery(): Query
    {
        $query = $this->createQueryBuilder('p')
            ->join('p.stock', 's')
            ->where('s.quantity < -1')
        ;

        return $query->getQuery();

    }


    /**
     * @return Query
     */
    public function findInStockQuery(): Query
    {
        $query = $this->createQueryBuilder('p')
            ->join('p.stock', 's')
            ->where('s.quantity > 0')
            ;

        return $query->getQuery();

    }

    /**
     * @param $research
     * @return Query
     */
    public function findReSearchQuery($research): Query
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.categories', 'c')
            ->where('p.description LIKE :reseach')
            ->orWhere('p.name LIKE :reseach')
            ->orWhere('c.name LIKE :reseach')
            ->setParameter('reseach', "%" . $research . "%")
            ;

        return $query->getQuery();
    }

    /**
     * @param $cumul
     * @param $catsChecked
     * @param $citiesChecked
     * @param $producersChecked
     * @return Query
     */
    public function findLeftSearchQuery($cumul, $catsChecked): Query
    {
        $query = $this->createQueryBuilder('p')
            ->leftJoin('p.categories', 'c')
        ;

        $query = $this->updateQuery($query, $catsChecked, $cumul, "c");

        return $query
            ->getQuery()
            ;
    }

    /**
     * @return Query
     */
    public function findSoldByType()
    {
        return $this->createQueryBuilder('p')
            ->join('p.categories', 'c')
            ->select('count(p), c.name')
            ->andWhere('o.order_date > :FDPW')
            ->andWhere('o.order_date < :LDPW')
            ->setParameter('FDPW', date("Y-n-j", strtotime("-2 week monday")))
            ->setParameter('LDPW', date("Y-n-j", strtotime("-1 week sunday")))
            ->addGroupBy('c.name')
            ->getQuery()
            ;
    }

    /**
     * @param $query
     * @param $updatersArray
     * @param $cumul
     * @param $alias
     * @return mixed
     */
    private function updateQuery(QueryBuilder $query, $updatersArray, $cumul, $alias) {
        foreach ($updatersArray as $updaterArray) {
            foreach ($updaterArray as $key => $value) {
                if ($value == "on") {
                    if ($cumul == "on") {
                        $query->andWhere($alias.'.name LIKE :'.$key);
                    } else {
                        $query->orWhere($alias.'.name LIKE :'.$key);
                    }
                    $query->setParameter($key, "%" . substr("$key", 0, -7) . "%");
                }
            }
        }

        return $query;
    }


    public function findBySimilarAt(Product $base) {

        return $this->createQueryBuilder('p')
            ->leftJoin('p.categories', 'c')
            ->where('c.id IN (:simili)')
            ->setParameter("simili", $base->getCategories())
            ->setMaxResults(5)
            ->getQuery()
            ->getResult();
    }
}
