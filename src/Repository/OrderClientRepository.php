<?php

namespace App\Repository;

use App\Entity\OrderClient;
use App\Entity\Product;
use App\Entity\Stock;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
//use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method OrderClient|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderClient|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderClient[]    findAll()
 * @method OrderClient[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderClientRepository extends ServiceEntityRepository
{
    /**
     * OrderClientRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderClient::class);
    }

    /**
     * @return mixed
     */
    public function findOrderClientLastWeek()
    {
        return $this->createQueryBuilder('o')
            ->innerJoin('o.products', 'op')
            ->select('count(op.id), o.order_date')
            ->andWhere('o.order_date > :FDPW')
            ->andWhere('o.order_date < :LDPW')
            ->setParameter('FDPW', date("Y-n-d", strtotime("-2 week monday")))
            ->setParameter('LDPW', date("Y-n-d", strtotime("-1 week sunday")))
            ->addGroupBy('o.order_date')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findSoldByType()
    {
        return $this->createQueryBuilder('o')
            ->join('o.products', 'p')
            ->join('p.categories', 'c')
            ->select('count(p), c.name')
            ->andWhere('o.order_date > :FDPW')
            ->andWhere('o.order_date < :LDPW')
            ->setParameter('FDPW', date("Y-n-d", strtotime("-2 week monday")))
            ->setParameter('LDPW', date("Y-n-d", strtotime("-1 week sunday")))
            ->addGroupBy('c.name')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return mixed
     */
    public function findSoldByProducer()
    {
        return $this->createQueryBuilder('o')
            ->join('o.products', 'p')
            ->join('p.producer', 'per')
            ->select('count(p), per.name')
            ->andWhere('o.order_date > :FDPW')
            ->andWhere('o.order_date < :LDPW')
            ->setParameter('FDPW', date("Y-n-d", strtotime("-2 week monday")))
            ->setParameter('LDPW', date("Y-n-d", strtotime("-1 week sunday")))
            ->addGroupBy('per.name')
            ->getQuery()
            ->getResult();
    }


    /**
     * @param array $cart
     * @param User $client
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setOrderClient(array $cart, User $client)
    {
        $em = $this->getEntityManager();

        $order = new OrderClient();

        $order->setClient($client);

        $order->setProducts(json_encode($cart));

        foreach ($cart as $product_id => $quantity){
            /**
             * @var $product Product
             */
            $product = $this->getEntityManager()
                ->getRepository(Product::class)
                ->find($product_id);
            /**
             * @var $stock Stock
             */
            $stock = $product->getStock();
            $stock->setQuantity($stock->getQuantity() - $quantity);

            $em->persist($stock);
            $em->persist($order);
        }

        $em->flush();
    }

    /**
     * @param User $client
     * @return mixed
     * @throws NonUniqueResultException
     */
    public function getLastOrderFor(User $client)
    {
        $query = $this->createQueryBuilder('o')
            ->select('o.id')
            ->join('o.client', 'c')
            ->where('c.id = :id')
            ->setParameter(':id', $client->getId())
            ->orderBy('o.id', 'DESC')
            ->setMaxResults(1)
            ;

        return $query
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
}
