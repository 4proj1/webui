<?php

namespace App\DataFixtures;

use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture
{
    private $passwordEncoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        // set Jane Admin
        $user = (new User())
            ->setFullName("Jane Doe")
            ->setUsername("jane_admin")
            ->setEmail("jane_admin@symfony.com")
            ->setRoles(['ROLE_ADMIN'])
            ->setIsBlocked(0)
            ->setRegisterAt(new DateTime(date("Y-m-d", strtotime("-2 year"))))
            ->setAddress("42 Avenue Admin 1337 Symfony")
        ;
        $user->setPassword($this->passwordEncoder->encodePassword($user, "kitten"));
        $manager->persist($user);
        $this->setReference($user->getUsername(), $user);

        // set All Clients
        for ($i = 0; $i < 40; $i++) {
            $user = (new User())
                ->setFullName($faker->lastName." ".$faker->firstName)
                ->setUsername($faker->userName)
                ->setEmail($faker->email)
                ->setRoles(['ROLE_USER'])
                ->setIsBlocked(0)
                ->setRegisterAt($faker->dateTimeBetween($startDate = '-1 month', $endDate = 'now'))
                ->setAddress($faker->streetAddress." ".$faker->postcode." ".$faker->city)
            ;
            $user->setPassword($this->passwordEncoder->encodePassword($user, "kitten"));

            $manager->persist($user);

            $this->setReference('User_'.$i, $user);
        }

        $manager->flush();
    }


}
