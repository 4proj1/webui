<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class CategoryFixture
 * @package App\DataFixtures
 */
class CategoryFixture extends Fixture
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $i = 1;
        foreach ($this->getCategoriesData() as [$name]) {
            $cat = new Category();
            $cat->setName($name);

            $manager->persist($cat);
            $this->setReference('Category_'.$i, $cat);
            $i++;
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    private function getCategoriesData(): array
    {
        return [
            // $categoriesData = [$name];
            ['Boissons'],
            ['Fruits'],
            ['Legumes'],
            ['Cereales'],
            ['Produits laitiers'],
            ['Viande'],
            ['Poisson'],
            ['Oeuf'],
            ['Corps_gras'],
            ['Sucre'],
        ];
    }

}
