<?php

namespace App\DataFixtures;

use App\Entity\OrderClient;
use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Exception;
use Faker\Factory;

/**
 * Class OrderClientFixture
 * @package App\DataFixtures
 */
class OrderClientFixture extends Fixture implements DependentFixtureInterface
{

    /**
     * @param ObjectManager $manager
     * @throws Exception
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 250; $i++) {
            $order = (new OrderClient())
                ->setOrderDate($faker->dateTimeBetween($startDate = '-1 month', $endDate = 'now'))
                ->setClient(
                    $this->getReference('User_' . $faker->numberBetween(0,39))
                )
            ;

            $cart_list = [];

            for ($k = 0; $k < $faker->numberBetween(1,5); $k++) {
                $product_id = $faker->numberBetween(0,999);
                $quantity = $faker->numberBetween(0,5);

                $cart_list[$product_id] = $quantity;
            }

            $order->setProducts(json_encode($cart_list));

            $manager->persist($order);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            ProductFixture::class,
        );
    }
}
