<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class  User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $full_name;

    /**
     * @ORM\Column(type="date_immutable", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $register_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $sex;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $diet;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $allergies;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_blocked;

    /**
     * @ORM\Column(type="json_array")
     */
    private $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderClient", mappedBy="client")
     */
    private $orderClients;


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * User constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->setRegisterAt(new DateTime());
        $this->full_name = $this->first_name . " " . $this->last_name;
        $this->orderClients = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->full_name;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    /**
     * @param string|null $last_name
     * @return $this
     */
    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    /**
     * @param string|null $first_name
     * @return $this
     */
    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->full_name;
    }

    /**
     * @param string|null $full_name
     * @return $this
     */
    public function setFullName(?string $full_name): self
    {
        $this->full_name = $full_name;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return DateTime|null
     */
    public function getBirthday(): ?DateTime
    {
        return $this->birthday;
    }

    /**
     * @param DateTime|null $birthday
     * @return $this
     */
    public function setBirthday(?DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return DateTime
     */
    public function getRegisterAt(): DateTime
    {
        return $this->register_at;
    }

    /**
     * @param DateTime $register_at
     * @return $this
     */
    public function setRegisterAt(DateTime $register_at): self
    {
        $this->register_at = $register_at;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return int|null
     */
    public function getSex(): ?int
    {
        return $this->sex;
    }

    /**
     * @param int|null $sex
     * @return $this
     */
    public function setSex(?int $sex): self
    {
        $this->sex = $sex;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return $this
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return mixed
     */
    public function getDiet()
    {
        return $this->diet;
    }

    /**
     * @param $diet
     * @return $this
     */
    public function setDiet($diet): self
    {
        $this->diet = $diet;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return mixed
     */
    public function getAllergies()
    {
        return $this->allergies;
    }

    /**
     * @param $allergies
     * @return $this
     */
    public function setAllergies($allergies): self
    {
        $this->allergies = $allergies;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return bool|null
     */
    public function getIsBlocked(): ?bool
    {
        return $this->is_blocked;
    }

    /**
     * @param bool $is_blocked
     * @return $this
     */
    public function setIsBlocked(bool $is_blocked): self
    {
        $this->is_blocked = $is_blocked;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param $roles
     * @return $this
     */
    public function setRoles($roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //              This methods is needed when UserInterface is used :
    //              "Class must be abstract or implement methods 'eraseCredentials','getSalt'"
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @return Collection|OrderClient[]
     */
    public function getOrderClients(): Collection
    {
        return $this->orderClients;
    }

    public function addOrderClient(OrderClient $orderClient): self
    {
        if (!$this->orderClients->contains($orderClient)) {
            $this->orderClients[] = $orderClient;
            $orderClient->setClient($this);
        }

        return $this;
    }

    public function removeOrderClient(OrderClient $orderClient): self
    {
        if ($this->orderClients->contains($orderClient)) {
            $this->orderClients->removeElement($orderClient);
            // set the owning side to null (unless already changed)
            if ($orderClient->getClient() === $this) {
                $orderClient->setClient(null);
            }
        }

        return $this;
    }
}