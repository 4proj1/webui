<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;


/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderClientRepository")
 */
class OrderClient
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $order_date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orderClients")
     * @ORM\JoinColumn(nullable=false)
     */
    private $client;

    /**
     * @ORM\Column(type="json_array")
     */
    private $products_list;


    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * OrderClient constructor.
     */

    public function __construct()
    {
        $this->products_list = new ArrayCollection();
        $this->order_date = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return null|DateTime
     */
    public function getOrderDate(): ?DateTime
    {
        return $this->order_date;
    }

    /**
     * @param DateTime $order_date
     * @return OrderClient
     * @throws Exception
     */
    public function setOrderDate(DateTime $order_date): self
    {
        $this->order_date = $order_date;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return User|null
     */
    public function getClient(): ?User
    {
        return $this->client;
    }

    /**
     * @param User|null $client
     * @return OrderClient
     */
    public function setClient(?User $client): self
    {
        $this->client = $client;

        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products_list;
    }

    /**
     * @param $products_list
     * @return $this
     */
    public function setProducts($products_list): self
    {
        $this->products_list = $products_list;

        return $this;
    }


}
