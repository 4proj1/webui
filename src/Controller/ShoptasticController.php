<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\CityRepository;
use App\Repository\ProducerRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * Class ShoptasticController
 * @package App\Controller
 */
class ShoptasticController extends AbstractController
{
    /**
     * @Route("/", name="shoptastic")
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param ProductRepository $repoProduct
     * @param CategoryRepository $repoCat
     * @param Query $queryProducts
     * @param string $catsChecked
     * @param string $producersChecked
     * @param string $cumul
     * @return Response
     */
    public function index(PaginatorInterface $paginator, Request $request, ProductRepository $repoProduct, CategoryRepository $repoCat, $queryProducts = null , $catsChecked = "", $producersChecked = "", $cumul = "")
    {
        if (is_null($queryProducts)) {
            $query = $repoProduct->findInStockQuery();
        }
        else {
            $query = $queryProducts;
        }

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $session = $this->get('session');
        $cart = $session->get('cart', array());
        ksort($cart);

        $cats = $repoCat->findAll();

        $catChecked = $this->setCheckedVar($catsChecked);
        $producerChecked = $this->setCheckedVar($producersChecked);

        return $this->render('shoptastic/index.html.twig', array(
            'cats' => $cats,
            'pagination' => $pagination,
            'catChecked' => $catChecked,
            'producerChecked' => $producerChecked,
            'cumul' => $cumul,
            'cart' => $cart,
        ));
    }

    private function setCheckedVar($arraysChecked) {

        $arrayChecked = array();

        if (is_array($arraysChecked) && !empty($arraysChecked)) {
            foreach ($arraysChecked as $item){
                foreach ($item as $key => $value) {
                    $arrayChecked[substr("$key", 0, -7)] = $value;
                }
            }
        }

        return $arrayChecked;

    }
}
