<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_index", methods={"GET"})
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @param ProductRepository $productRepository
     * @param string $pagination
     * @return Response
     */
    public function index(PaginatorInterface $paginator,  Request $request, ProductRepository $productRepository, $pagination = ""): Response
    {
        if (!empty($pagination)) {
            $pagination = $paginator->paginate(
                $productRepository->findInStockQuery(), /* query NOT result */
                $request->query->getInt('page', 1), /*page number*/
                10 /*limit per page*/
            );
        }

        return $this->render('product/index.html.twig', ['pagination' => $pagination]);
    }

    /**
     * @Route("/{id}", name="product_show", methods={"GET"})
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {

        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @param ProductRepository $repo
     * @param Product $product
     * @return Response
     */
    public function similarProduct(ProductRepository $repo, Product $product) {
        return $this->render('product/simili.html.twig', [
            'products' => $repo->findBySimilarAt($product),
            'product' => $product
        ]);
    }
}
