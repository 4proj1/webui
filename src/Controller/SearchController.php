<?php

namespace App\Controller;

use App\Repository\CategoryRepository;
use App\Repository\CityRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class SearchController
 * @package App\Controller
 */
class SearchController extends AbstractController
{

    /**
     * @return Response
     */
    public function searchBar()
    {
        $form = $this->createFormBuilder()
            ->add('search', SearchType::class, array('constraints' => new Length(array('min' => 3)), 'attr' => array('placeholder' => 'Rechercher ...')))
            ->getForm();

        return $this->render('shoptastic/search/searchBar.html.twig', array(
            'form' => $form->createView()
        ));

    }

    /**
     * @Route("/search", name="search")
     * @param Request $request
     * @param ProductRepository $repo
     * @return Response
     */
    public function Research(Request $request, ProductRepository $repo)
    {

        $form = $this->createFormBuilder()
            ->add('search', SearchType::class, array('constraints' => new Length(array('min' => 3)), 'attr' => array('placeholder' => 'Rechercher ...')))
            ->getForm();

        $form->handleRequest($request);

        $query = "";

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $reseach = $data["search"];

            $query = $repo->findReSearchQuery($reseach);
        }

        return $this->forward('App\\Controller\\ShoptasticController::index', array(
            'queryProducts'  => $query,

        ));

    }

    /**
     * @Route("/leftsearch", name="leftsearch")
     * @param Request $request
     * @param ProductRepository $repoProduct
     * @param CategoryRepository $repoCat
     * @return Response
     */
    public function leftBarFilter(Request $request, ProductRepository $repoProduct, CategoryRepository $repoCat)
    {
        $cumul =$request->get("cumul");
        $cats = $repoCat->findAll();
        $query = "";
        $catsChecked = [];

        foreach ($cats as $cat) {
            array_push($catsChecked, [$cat->getName()."Checked" => $request->get($cat->getName())]);
        }

        $query = $repoProduct->findLeftSearchQuery($cumul, $catsChecked);

        return $this->forward('App\\Controller\\ShoptasticController::index', array(
            'queryProducts' => $query,
            'catsChecked' => $catsChecked,
            'cumul' => $cumul,
        ));
    }

}
