<?php

namespace App\Controller;

use App\Entity\OrderClient;
use App\Entity\User;
use App\Entity\Product;
use App\Repository\OrderClientRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Process\Exception\ProcessTimedOutException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

/**
 * @Route("/cart")
 */
class CartController extends AbstractController
{
    /**
     * @Route("/", name="cart_index", methods={"GET"})
     * @return Response
     */
    public function show(): Response
    {
        $session = $this->get('session');
        $cart = $session->get('cart', array());
        ksort($cart);

        return $this->render('cart/show.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     * @Route("/add/{id_product}", name="cart_add", methods={"GET","POST"})
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function add(Request $request, ProductRepository $productRepository): Response
    {
        $nbBuy = $request->get("nbBuy");
        list($session, $route, $cart, $product) = $this->getCart($request, $productRepository);

        if (!empty($nbBuy)){
            for ($i=0;$i<$nbBuy-1;$i++){
                array_push($cart, $product);
            }
        }
        array_push($cart, $product);


        $session->set('cart', $cart);

        return $this->redirect($route);
    }

    /**
     * @Route("/delete/{id_product}", name="cart_delete", methods={"GET"})
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return Response
     */
    public function delete(Request $request, ProductRepository $productRepository): Response
    {
        list($session, $route, $old_cart, $wanted_product) = $this->getCart($request, $productRepository);
        $deleted = false;
        $cart = array();

        foreach ($old_cart as $product) {
            if ($wanted_product->getId() == $product->getId() && !$deleted) {
                $deleted = true;
                continue;
            } else {
                array_push($cart, $product);
            }
        }

        $session->set('cart', $cart);
        return $this->redirect($route);
    }

    /**
     * @Route("/buy", name="cart_buy")
     * @param Pdf $snappy
     * @param OrderClientRepository $clientRepository
     * @return Response
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function buy(Pdf $snappy, OrderClientRepository $clientRepository)
    {
        $session = $this->get('session');
        $cart = $session->get('cart', array());
        ksort($cart);

        /**
         * @var $client User
         */

        $client = $this->getUser();

        $out_cart = array();

        /**
         * @var $product Product
         */
        foreach ($cart as $product) {
            array_push($out_cart, $product->getId());
        }

        $new_cart = array_count_values($out_cart);

        $clientRepository->setOrderClient($new_cart, $client);

        $session->set('cart', array());

//        return $this->getPDF($snappy, $cart);
        return $this->render('cart/show.html.twig', [
            'cart' => $cart
        ]);
    }

    /**
     * Export to PDF
     * @param Pdf $snappy
     * @param $cart
     * @return Response
     * @throws NonUniqueResultException
     */
    private function getPDF(Pdf $snappy, $cart): Response
    {
        $em = $this->getDoctrine()->getManager();

        $clientRepo = $em->getRepository(OrderClient::class);

        $lastOrder = $clientRepo->getLastOrderFor($this->getUser());

        $snappy->setTimeout(30);

        $facNb = date("Ymd") . '-'.$lastOrder;

        $htmlview = $this->render('cart/pdf.html.twig', [
            'cart' => $cart,
            'facNb' => $facNb
        ]);

        try {
            $pdfRes = new PdfResponse(
                $snappy->getOutputFromHtml($htmlview),
                'Facture_No.' . $facNb . '.pdf'
            );
            $response = $pdfRes;
        } catch (ProcessTimedOutException $to) {
            sleep(2);
        }

        if (empty($pdfRes)) {
            $cmd = new Process("ls -ct /tmp/ | grep .pdf | head -1");
            $cmd->run();
            $pdf = "/tmp/" . $cmd->getOutput();
            $pdf = str_replace("\n", "", $pdf);

            $response = new BinaryFileResponse($pdf);
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                'Facture_No.' . $facNb . '.pdf'
            );

        }

        return $response;
    }

    /**
     * @param Request $request
     * @param ProductRepository $productRepository
     * @return array
     */
    private function getCart(Request $request, ProductRepository $productRepository)
    {
        $session = $this->get('session');
        $route = $request->headers->get('referer');

        $id_product = $request->attributes->get('id_product');
        $product = $productRepository->find($id_product);

        $cart = $session->get('cart', array());

        return array($session, $route, $cart, $product);
    }

}
