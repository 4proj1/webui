<?php

namespace App\Controller;

use App\Repository\OrderClientRepository;
use App\Repository\UserRepository;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\ColumnChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AnalyticsController
 * @package App\Controller
 */
class AnalyticsController extends AbstractController
{
    /**
     * @Route("/admin/analytics", name="analytics")
     * @param UserRepository $userRepo
     * @param OrderClientRepository $orderRepo
     * @return Response
     */
    public function indexAction(UserRepository $userRepo, OrderClientRepository $orderRepo)
    {
        $userChart = $this->getNbUser($userRepo);
        $PSLWChart = $this->getNbProductSoldLastWeek($orderRepo);
        $PSBTChart = $this->getNbProductSoldByType($orderRepo);
        $PSBPChart = $this->getNbProductSoldByProducer($orderRepo);

        return $this->render('analytics/index.html.twig', array(
            'nbUsers' => $userChart,
            'nbProductSoldLastWeek' => $PSLWChart,
            'nbProductSoldByType' => $PSBTChart,
            'nbProductSoldByProducer' => $PSBPChart

        ));
    }

    /**
     * @param UserRepository $repository
     * @return ColumnChart
     */
    private function getNbUser(UserRepository $repository) {
        $nbUsers = $repository->findRegisterLastM();
        $data = $this->formateData(array(['date', 'Nb User']), $nbUsers, "month");

        $chart = new ColumnChart();
        $chart->getData()->setArrayToDataTable($data);
        $chart->getOptions()->setTitle("Nouveaux clients du mois dernier");
        $chart->getOptions()->getLegend()->setPosition('none');

        $chart->getOptions()->setHeight(500);

        return $chart;
    }

    /**
     * @param OrderClientRepository $repository
     * @return ColumnChart
     */
    private function getNbProductSoldLastWeek(OrderClientRepository $repository)
    {
        $nbOrders = $repository->findOrderClientLastWeek();
        $data = $this->formateData(array(['date', 'Nb Product']), $nbOrders, "week");

        $bweel = date("d/m/Y", strtotime("-2 week monday"));
        $eweel = date("d/m/Y", strtotime("-1 week sunday"));

        $chart = new ColumnChart();
        $chart->getData()->setArrayToDataTable($data);
        $chart->getOptions()->getLegend()->setPosition('none');
        $chart->getOptions()->setTitle("Nombre de produits vende a S-1 (de ".$bweel." à ".$eweel.")");

        $chart->getOptions()->setHeight(500);

        return $chart;
    }

    /**
     * @param OrderClientRepository $repository
     * @return PieChart
     */
    private function getNbProductSoldByType(OrderClientRepository $repository)
    {
        $productByType = $repository->findSoldByType();
        $data = $this->formateData(array(['Categorie', 'Nb Product']), $productByType, "week", false);

        $chart = new PieChart();
        $chart->getData()->setArrayToDataTable($data);
        $chart->getOptions()->setTitle("Répartition des ventes par Catégories");
        $chart->getOptions()->setIs3D(true);
        $chart->getOptions()->setHeight(500);

        return $chart;
    }

    /**
     * @param OrderClientRepository $repository
     * @return PieChart
     */
    private function getNbProductSoldByProducer(OrderClientRepository $repository)
    {
        $productByProducer = $repository->findSoldByProducer();
        $data = $this->formateData(array(['Categorie', 'Nb Product']), $productByProducer, "week", false);

        $chart = new PieChart();
        $chart->getData()->setArrayToDataTable($data);
        $chart->getOptions()->setTitle("Répartition des ventes par Producteur");
        $chart->getOptions()->setWidth(500);
        $chart->getOptions()->setIs3D(true);

        return $chart;
    }

    /**
     * @param $header
     * @param $rawData
     * @param $range
     * @param bool $isdate
     * @return mixed
     */
    private function formateData($header, $rawData, $range, $isdate = true)
    {
        if (empty($rawData)) {
            array_push($header, [date("Y-n", strtotime("previous " . $range)), 0]);
        } else {
            foreach ($rawData as $item) {
                $array = array_values($item);
                $nb = (int)$array[0];
                if ($isdate) {
                    $by = $array[1]->format('d/m/Y');
                } else {
                    $by = $array[1];
                }
                array_push($header, [$by, $nb]);
            }
        }

        return $header;
    }

}
