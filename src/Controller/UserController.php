<?php

namespace App\Controller;

use App\Entity\OrderClient;
use App\Entity\Product;
use App\Entity\User;
use App\Form\UserType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/mobileLogin", name="user_get", methods={"GET"})
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function mobileLogin(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $username = $request->query->get("username");
        $password = $request->query->get("password");

        $user = $this->getDoctrine()->getManager()->getRepository(User::class)->findOneBy(['username' => $username]);

        if($user != null and $passwordEncoder->isPasswordValid($user, $password)){
            return $response = new Response(strval($user->getId()));
        }

        return $response = new Response('KO');
    }

    /**
     * @Route("/myOrder", name="user_order", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function myOrder(Request $request): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        $orders_list = [];
        /**
         * @var OrderClient $orders
         */
        foreach ($user->getOrderClients() as $orders) {
            $removed = array("\"","{","}");
            $tmp_product = $orders->getProducts();

            if (!is_string($tmp_product)){
                $tmp_product = json_encode($tmp_product);
            }

            $products = explode(",", str_replace($removed, "", $tmp_product));

            foreach ($products as $product_detail) {
                list($product_id, $quantity) = explode(":", $product_detail);
                /**
                 * @var Product $product
                 */
                $product =
                    $this->getDoctrine()->getManager()->getRepository(Product::class)->find($product_id);
                array_push($orders_list, ["oid_".$orders->getId(), $product, $quantity]);
            }
        }

        return $this->render('user/my_orders.html.twig', [
            'orders_list' => $orders_list,
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        $gravatar_hash = md5(strtolower(trim("rootkit@rootkit-lab.org")));
        //$gravatar_hash = md5(strtolower(trim($user->getEmail())));

        if ($user->getId() == $this->getUser()->getId()) {
            return $this->render('user/show.html.twig', [
                'user' => $user,
                'hash' => $gravatar_hash,
            ]);
        } else {
            throw new UnauthorizedHttpException("Nop", "Nop");
        }
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @param Request $request
     * @param User $user
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws Exception
     */
    public function edit(Request $request, User $user,UserPasswordEncoderInterface $passwordEncoder): Response
    {
        if ($user->getId() == $this->getUser()->getId()) {
            $form = $this->createForm(UserType::class, $user);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $plainPass = $form["password"]->getData();
                $password = $passwordEncoder->encodePassword($user, $plainPass);
                $user->setPassword($password);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();

                return $this->redirectToRoute('shoptastic');
            }

            return $this->render('user/edit.html.twig', [
                'user' => $user,
                'form' => $form->createView(),
            ]);
        } else {
            throw new UnauthorizedHttpException("Nop", "Nop");
        }

    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('shoptastic');
    }

}
