SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `dbusers` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

GRANT ALL ON dbusers.* TO 'admin'@'%';
FLUSH PRIVILEGES;

USE `dbusers`;

create table category
(
    id   int auto_increment
        primary key,
    name varchar(255) not null
)
    collate = utf8mb4_unicode_ci;

create table migration_versions
(
    version     varchar(14) not null
        primary key,
    executed_at datetime    not null comment '(DC2Type:datetime_immutable)'
)
    collate = utf8mb4_unicode_ci;

create table user
(
    id          int auto_increment
        primary key,
    last_name   varchar(255)                 null,
    first_name  varchar(255)                 null,
    full_name   varchar(255)                 null,
    birthday    date                         null comment '(DC2Type:date_immutable)',
    register_at date                         null,
    username    varchar(255)                 not null,
    sex         smallint                     null,
    email       varchar(255)                 null,
    phone       varchar(255)                 null,
    address     varchar(255)                 null,
    diet        longtext collate utf8mb4_bin null comment '(DC2Type:json_array)',
    allergies   longtext collate utf8mb4_bin null comment '(DC2Type:json_array)',
    password    varchar(255)                 not null,
    is_blocked  tinyint(1)                   not null,
    roles       longtext collate utf8mb4_bin not null comment '(DC2Type:json_array)',
    constraint allergies
        check (json_valid(`allergies`)),
    constraint diet
        check (json_valid(`diet`)),
    constraint roles
        check (json_valid(`roles`))
)
    collate = utf8mb4_unicode_ci;

create table order_client
(
    id            int auto_increment
        primary key,
    client_id     int                          not null,
    order_date    date                         not null,
    products_list longtext collate utf8mb4_bin not null comment '(DC2Type:json_array)',
    constraint FK_4CB148019EB6921
        foreign key (client_id) references user (id),
    constraint products_list
        check (json_valid(`products_list`))
)
    collate = utf8mb4_unicode_ci;

create index IDX_4CB148019EB6921
    on order_client (client_id);

create table provider
(
    id         int auto_increment
        primary key,
    contact_id int          not null,
    siret      varchar(14)  not null,
    name       varchar(255) not null,
    address    varchar(255) null,
    is_blocked tinyint(1)   not null,
    constraint UNIQ_92C4739CE7A1254A
        unique (contact_id),
    constraint FK_92C4739CE7A1254A
        foreign key (contact_id) references user (id)
)
    collate = utf8mb4_unicode_ci;

create table product
(
    id          int auto_increment
        primary key,
    provider_id int          null,
    name        varchar(255) not null,
    price       double       not null,
    image       varchar(255) not null,
    description longtext     not null,
    constraint FK_D34A04ADA53A8AA
        foreign key (provider_id) references provider (id)
)
    collate = utf8mb4_unicode_ci;

create index IDX_D34A04ADA53A8AA
    on product (provider_id);

create table product_category
(
    product_id  int not null,
    category_id int not null,
    primary key (product_id, category_id),
    constraint FK_CDFC735612469DE2
        foreign key (category_id) references category (id)
            on delete cascade,
    constraint FK_CDFC73564584665A
        foreign key (product_id) references product (id)
            on delete cascade
)
    collate = utf8mb4_unicode_ci;

create index IDX_CDFC735612469DE2
    on product_category (category_id);

create index IDX_CDFC73564584665A
    on product_category (product_id);

create table stock
(
    id         int auto_increment
        primary key,
    product_id int not null,
    quantity   int not null,
    constraint UNIQ_4B3656604584665A
        unique (product_id),
    constraint FK_4B3656604584665A
        foreign key (product_id) references product (id)
)
    collate = utf8mb4_unicode_ci;

