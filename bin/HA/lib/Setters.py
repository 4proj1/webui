#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from lib import Getters


def set_mail():
	smtp = Getters.get_config()['SMTP']
	contact = Getters.get_config()['MAIL']

	s = smtplib.SMTP(host=smtp['host'], port=smtp['port'])
	s.starttls()
	s.login(contact['mailfrom'], contact['mailfrom_password'])

	return s


def set_msg():
	contact = Getters.get_config()['MAIL']
	conf = Getters.get_config()['REMOTE']

	msg_tpl = Getters.get_template(Getters.get_bm_path() + "config/message.txt")
	msg = MIMEMultipart()

	message = msg_tpl.substitute(HOST_NAME=conf['host'])

	msg['From'] = contact['mailfrom']
	msg['To'] = contact['mailto']
	msg['Subject'] = "ALERTE unreachable host"

	msg.attach(MIMEText(message, 'plain'))

	return msg
