#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import configparser
from string import Template


def get_bm_path():
	return os.getcwd()+"/"


def get_config():
	config = configparser.ConfigParser()
	config.read(get_bm_path()+"config/conf.cfg")
	return config


def get_template(filename):
	with open(filename, 'r', encoding='utf-8') as template_file:
		template_file_content = template_file.read()
	return Template(template_file_content)
