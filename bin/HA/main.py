#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from time import sleep

from lib import Getters, Mail


def main():
	conf = Getters.get_config()['REMOTE']
	while os.system("ping -c 1 " + conf['host']) is 0:
		sleep(1)

	Mail.send_mail()


if __name__ == '__main__':
	main()

