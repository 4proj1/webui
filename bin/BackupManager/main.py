#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lib import Getters, Setters


def main():
	action = Getters.get_opts()

	if action in ["data", "sql"]:
		getattr(Setters, 'set_tarball_%s' % action)()
	else:
		print("Unknown type")


if __name__ == '__main__':
	main()
