#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import configparser
import getopt
import os
import sys
from string import Template


def get_bm_path():
	return os.getcwd() + "/"


def get_foodtatic_path():
	return get_bm_path() + 'Foodtastic/'


def get_project_path():
	return os.path.abspath(os.path.join(os.path.abspath(os.path.join(get_bm_path(), os.pardir)), os.pardir))+"/"


def get_config():
	config = configparser.ConfigParser()
	config.read(get_bm_path() + "config/conf.cfg")
	return config


def get_template(filename):
	with open(filename, 'r', encoding='utf-8') as template_file:
		template_file_content = template_file.read()
	return Template(template_file_content)


def get_opts():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ht:", ["help", "type"])
	except getopt.GetoptError as err:
		# print help information and exit:
		print(err)  # will print something like "option -a not recognized"
		sys.exit(2)

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			sys.exit()
		elif opt in ("-t", "--type"):
			return arg
		else:
			assert False, "unhandled option"
