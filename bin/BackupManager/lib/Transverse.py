#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lib import Getters, Setters, Mail

import os


def send_archive(file):
	scp = Setters.set_scp_conf()
	if "sql" in file:
		remote = "sql"
	else:
		remote = "data"

	try:
		scp.put(file+".gz", remote_path='/backup/' + remote)
	except Exception as e:
		Mail.send_mail(e)
		exit(3)
	scp.close()
	post_traitement()


def post_traitement():
	for file in os.listdir(Getters.get_foodtatic_path()):
		try:
			os.remove(Getters.get_foodtatic_path()+file)
		except Exception as e:
			Mail.send_mail(e)
