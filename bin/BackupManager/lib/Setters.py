#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import datetime
import os
import pipes
import smtplib
import tarfile

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from paramiko import SSHClient
from scp import SCPClient

from lib import Getters, Mail, Transverse


def set_tarball_sql():
	sql_config = Getters.get_config()['SQL']
	today = datetime.date.today()
	output = Getters.get_foodtatic_path() + sql_config['db'] + ".sql"
	dump_cmd = "mysqldump  -u " + sql_config['user'] + " -p" + sql_config['password'] + " " + sql_config['db'] + " > " \
			  + pipes.quote(output)

	os.system(dump_cmd)

	tar_output = Getters.get_foodtatic_path() + "Foodtastic_sql_" + today.strftime("%Y%m%d") + ".tar"
	backup = tarfile.open(tar_output, "a:")
	try:
		backup.add(output)
	except Exception as e:
		Mail.send_mail(e)
		exit(3)
	gzip_cmd = "gzip " + pipes.quote(tar_output)

	os.system(gzip_cmd)
	Transverse.send_archive(tar_output)


def set_tarball_data():
	now = datetime.datetime.now()
	output = Getters.get_foodtatic_path() + "Foodtastic_" + now.strftime("%Y%m%d_%H%M") + ".tar"
	backup = tarfile.open(output, "a:")
	try:
		backup.add(Getters.get_project_path() + "src")
		backup.add(Getters.get_project_path() + "templates")
		backup.add(Getters.get_project_path() + "public/foodtastic")
		backup.add(Getters.get_project_path() + "public/materialize")
		backup.add(Getters.get_project_path() + "composer.json")
	except Exception as e:
		Mail.send_mail(e)
		exit(3)
	gzip_cmd = "gzip " + pipes.quote(output)

	os.system(gzip_cmd)
	Transverse.send_archive(output)


def set_scp_conf():
	backup_config = Getters.get_config()['BACKUP']
	ssh = SSHClient()
	ssh.load_host_keys(os.path.expanduser(backup_config['know_host']))
	try:
		ssh.connect(hostname=backup_config['host'], username=backup_config['user'], password=backup_config['password'])
	except Exception as e:
		Mail.send_mail(e)
		exit(3)

	# SCPClient takes a paramiko transport as an argument
	scp = SCPClient(ssh.get_transport())
	return scp


def set_mail():
	smtp = Getters.get_config()['SMTP']
	contact = Getters.get_config()['MAIL']

	s = smtplib.SMTP(host=smtp['host'], port=smtp['port'])
	s.starttls()
	s.login(contact['mailfrom'], contact['mailfrom_password'])

	return s


def set_msg(Traceback):
	contact = Getters.get_config()['MAIL']

	msg_tpl = Getters.get_template(Getters.get_bm_path() + "config/message.txt")
	msg = MIMEMultipart()

	message = msg_tpl.substitute(E=Traceback)

	msg['From'] = contact['mailfrom']
	msg['To'] = contact['mailto']
	msg['Subject'] = "ALERTE Error in backup process"

	msg.attach(MIMEText(message, 'plain'))

	return msg
