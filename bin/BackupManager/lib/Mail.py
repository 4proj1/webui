#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from lib import Setters


def send_mail(error):
	s = Setters.set_mail()
	msg = Setters.set_msg(error)

	s.send_message(msg)
	del msg
	s.quit()
