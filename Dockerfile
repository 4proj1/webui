FROM bitnami/symfony

COPY . /app/myapp/

WORKDIR /app/myapp

CMD php /app/myapp/bin/console assets:install; php /app/myapp/bin/console server:run 0.0.0.0:8000